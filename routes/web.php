<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*****************************
*
* Frontend
*
*/
Route::get('/', 'HomeController@index')->name('home');
Route::get('/search', 'SearchController@filter')->name('search');

/* ----------------------
	jobs
*/
Route::get('/jobs/{job}', 'JobController@show')->name('job.show');
Route::post('/jobs/{job}', 'JobController@sendJobApplication')->name('job.send');


/*****************************
*
* Authentication
*
*/
Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

/* ----------------------
	social auth
*/
Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');

/*****************************
*
* Backend
*
*/
Route::get('/dashboard', 'BackendController@index')->name('dashboard');

Route::middleware(['roles:admin'])->group(function () {

	/* ----------------------
		users + roles
	*/
	Route::get('/dashboard/users', 'BackendController@users')->name('users');
	Route::post('/dashboard/users/assign-roles', 'BackendController@postAdminAssignRoles')->name('admin.assign');
});

Route::middleware(['roles:employer'])->group(function () {

	/* ----------------------
		jobs
	*/
	Route::get('/dashboard/jobs', 'JobController@index')->name('job.index');
	Route::get('/dashboard/jobs/create', 'JobController@create')->name('job.create');
	Route::post('/dashboard/jobs', 'JobController@store');
	Route::get('/dashboard/jobs/{job}/edit', 'JobController@edit')->name('job.edit');
	Route::put('/dashboard/jobs/{job}', 'JobController@update');
	Route::delete('dashboard/jobs/{job}', 'JobController@destroy');
	Route::get('dashboard/jobs/{job}', 'JobController@toggleIsActive')->name('job.isActive');
});

Route::middleware(['roles:employee'])->group(function () {

	/* ----------------------
		profile
	*/
	Route::get('/dashboard/profile', 'ProfileController@index')->name('profile.index');
	Route::get('/dashboard/profile/create', 'ProfileController@create')->name('profile.create');
	Route::post('/dashboard/profile', 'ProfileController@store')->name('profile.store');
	Route::get('/dashboard/profile/{profile}/edit', 'ProfileController@edit')->name('profile.edit');
	Route::put('/dashboard/profile/{profile}', 'ProfileController@update');
	Route::delete('/dashboard/profile/{profile}', 'ProfileController@destroy');
});


