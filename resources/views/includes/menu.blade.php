<nav class="navbar navbar-toggleable-md navbar-light">
    {{-- NAVBAR TOGGLER --}}
    <button class="navbar-toggler navbar-toggler-right">
      <span class="navbar-toggler-icon"></span>
    </button>
    {{-- NAVBAR CLOSE TOGGLER --}}
     <button class="navbar-toggler-close">
      &times;
    </button>

    {{-- LOGO --}}
  @if(Request::url() == URL::route('home') || Request::url() == URL::route('search'))
    <div id="circle-background-home"></div>
  @endif
  <div id="circle"></div>
  <div class="navbar-brand-centered">
    <a class="navbar-brand" href="{{ URL::to('/') }}">
      <img src="{{ URL::to('/') }}/images/logo.png" alt="logo">
    </a>
  </div>
  
  {{-- collapsing nav --}}
  <div id="main-nav" class="collapse navbar-collapse">
    <div class="container">
      <ul class="navbar-nav">
        @if (Auth::check())
          <li class="nav-item">
            <a href="{{ URL::route('dashboard') }}" class="nav-link">
            Dashboard
            </a>
          </li>
          <li class="nav-item login-button">
            <a href="{{ route('logout') }}" class="nav-link">
            Logout
            </a>
          </li>
        @else
          <li class="nav-item">
            <a href="{{ route('register') }}" class="nav-link">
              Register
            </a>
          </li>
          <li class="nav-item login-button">
            <a href="{{ route('login') }}" class="nav-link">
              Login
            </a>
          </li>
        @endif
      </ul>
    </div>
  </div>
</nav>





  


