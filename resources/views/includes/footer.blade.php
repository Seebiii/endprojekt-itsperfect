<footer>
	<p>Terms &amp; Conditions</p>
</footer>
<script src="https://use.fontawesome.com/fa5275dcfe.js"></script>
<script src="{{ mix("js/app.js") }}"></script>
<script>
	tinymce.init({ 
		selector: 'textarea',
		menubar: false,
		toolbar: 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat',
		content_style: ".mce-content-body {background: #101723; color: #fff;}"
   	});
</script>