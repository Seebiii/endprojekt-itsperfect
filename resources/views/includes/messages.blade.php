@if(Session::has('success'))
	<div class="alert alert-success" role="alertt">
		<strong>Success:</strong> {{ Session::get('success') }}
	</div>
@endif