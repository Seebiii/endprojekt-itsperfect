<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="{{ mix("css/app.css") }}">

<title>ITsPerfect - @yield('title')</title>

<script>window.Laravel = { csrfToken: '{{ csrf_token() }}' };</script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ljhsuyccddtxy7qd77dr96yhmzp63g9qnpge7vawpfhrwhvs"></script>
