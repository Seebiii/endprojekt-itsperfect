<nav class="sidebar">
	<div class="navbar-brand-centered">
		<a class="navbar-brand" href="{{ URL::to('/') }}">
			<img src="{{ URL::to('/') }}/images/logo_light.svg" alt="logo">
		</a>
	</div>
	<ul class="nav">
		<li class="nav-item">
			<a href="{{ URL::route('dashboard') }}" class="nav-link">
				<i class="fa fa-tachometer" aria-hidden="true"></i>
				Dashboard
			</a>
		</li>

		@if(Auth::user()->isRole('admin'))
			<li class="nav-item">
				<a href="{{ URL::route('users') }}" class="nav-link">
					<i class="fa fa-users" aria-hidden="true"></i>
					Users
				</a>
			</li>
		@elseif(Auth::user()->isRole('employee'))
			<li class="nav-item">
				<a href="{{ URL::route('home') }}" class="nav-link">
					<i class="fa fa-search" aria-hidden="true"></i>
					Job search
				</a>
			</li>
			<li class="nav-item">
				<a href="{{ URL::route('profile.index') }}" class="nav-link">
					<i class="fa fa-user" aria-hidden="true"></i>
					Profile
				</a>
			</li>
		@elseif(Auth::user()->isRole('employer'))
			<li class="nav-item">
				<a href="{{ URL::route('home') }}" class="nav-link">
					<i class="fa fa-search" aria-hidden="true"></i>
					Job search
				</a>
			</li>
			<li class="nav-item">
				<a href="{{ URL::route('job.index') }}" class="nav-link">
					<i class="fa fa-briefcase" aria-hidden="true"></i>
					Jobs
				</a>
			</li>
		@endif
		<li class="nav-item login-button">
			<a href="{{ route('logout') }}" class="nav-link">
				<i class="fa fa-sign-out" aria-hidden="true"></i>
				Logout
			</a>
		</li>
	</ul>
</nav>