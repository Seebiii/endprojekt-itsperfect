<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ $subject }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
</head>
<body style="font-family: 'lato', Helvetica, Arial, sans-serif;">
	
	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
		<tr>
			<td align="center" valign="top">
				<table border="0" cellpadding="20" cellspacing="0" width="800" id="emailContainer">
		               <tr>
						<td align="center" valign="top">
						    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="emailBody">
					        		<tr>
					            		<td align="center" valign="top">
					                		<h3 style="text-align: center; background: #de6534; padding: 20px 0; color: #fff; text-transform: uppercase; margin: 0;">Job Application for<br>"{{ $subject }}"</h3>

										<div style="padding: 20px 0; text-align: left;">{!! $msg !!}</div>
					            		</td>
					        		</tr>
						    </table>
						</td>
					</tr>
					<tr>
	                   		<td align="center" valign="top">
		                        	<table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailFooter">
		                        		<tr style="background: #263248; color: #fff;">
		                        			<th>Name</th>
		                        			<th>State</th>
		                        			<th>Good knowledge</th>
		                        			<th>Average knowledge</th>
		                        		</tr>
		                           	<tr>
		                           		<td align="center" valign="top">{{ $name }} {{ $surname }}</td>
		                           		<td align="center" valign="top">{{ $state }}</td>
		                                	<td align="center" valign="top">
										<ul style="margin: 0;list-style: none;padding: 0;">
											@foreach($good_knowledge as $knowledge)
												<li>{{ $knowledge }}</li>
											@endforeach
										</ul>
									</td>
									<td align="center" valign="top">
										<ul style="margin: 0;list-style: none;padding: 0;">
											@foreach($average_knowledge as $knowledge)
												<li>{{ $knowledge }}</li>
											@endforeach
										</ul>
		                                	</td>
		                            	</tr>
		                        </table>
		                    </td>
		               </tr>
				</table>
			</td>
		</tr>
	</table>

	
</body>
</html>
