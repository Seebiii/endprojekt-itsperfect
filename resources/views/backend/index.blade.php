@extends('layouts.backend')

@section('title', 'Dashboard')

@section('content')

<div class="container col-xl-8 col-lg-8 col-md-10">
    
    <h1>Dashboard</h1>

    @if(Auth::user()->isRole('employee'))
        @if(!isset($profile))
            <p>Du hast noch kein Profil?! <a class="" href="{{ route('profile.create') }}">Hier</a> erstellen!</p>
        @endif
    @endif

</div>
@endsection
