@extends('layouts.backend')

@section('title', 'Edit profile')

@section('content')

<div id="form-container-dark" class="container">
    <div class="container col-xl-8 col-lg-8 col-md-10">
        <a href="{{ URL::route('profile.index') }}" class="btn btn-primary backend-back-button">back to profile overview</a>
		<h1>Edit profile</h1>
        
		<form action="{{ action('ProfileController@update', $profile) }}" method="POST" enctype="multipart/form-data">
            
            {{ method_field('PUT') }}
			{{ csrf_field() }}
            
            @include('layouts.errors')
            
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" value="{{ $profile->name }}">
                </div>
                <div class="form-group col-md-6">
                    <label for="surname">Surname</label>
                    <input type="text" class="form-control" name="surname" value="{{ $profile->surname }}">
                </div>
            </div>
            <div class="row">
    		  	<div class="form-group col-md-5">
    			    <label for="state">State</label>
    			    <select name="state" class="custom-select">
    			    	<option value="" disabled>Please choose ...</option>
                        @foreach($states as $state)
                            <option value="{{$state->id}}" {{ ($state->id == $profile->state_id) ? 'selected="selected"' : "" }}>
                                {{ ucfirst($state->name) }}
                            </option>
                        @endforeach
    			    </select>
    		  	</div>
                <div class="form-group col-md-2">
                    <label for="age">Age</label>
                    <input type="number" class="form-control" name="age" value="{{ $profile->age }}">
                </div>
                <div class="form-group col-md-5">
                    <input type="file" name="curriculum_vitae" id="file" class="inputfile">
                    <label for="file" class="col-md-12 file-upload">
                        <i class="fa fa-upload" aria-hidden="true"></i>
                        upload curriculum vitae
                    </label>
               </div>
            </div>
			<div class="form-group col-md-12">
				<label for="about_me">About me</label>
		    	<textarea class="form-control" rows="3" name="about_me">{{ $profile->about_me }}</textarea>
			</div>
            <div class="row">
    			<div class="form-group col-md-6 dropdownContainer">
    				<button type="button" class="hasDropdown">
                        <span class="dropdownLabel">Good knowledge</span>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </button>
    				<div class="selectGroup">
                        @foreach($technologies as $technology)
                            <label>
                                <input name="good_knowledge[]" type="checkbox" value="{{ $technology->id }}"
                                @for($i = 0; $i < count($profile->good_knowledge); $i++ )
                                    {{ ($technology->id == $profile->good_knowledge[$i]) ? 'checked="checked"' : '' }}
                                @endfor>
                                <span>{{ $technology->name }}</span>
                            </label>
                        @endforeach
                    </div>
    			</div>
    			<div class="form-group col-md-6 dropdownContainer">
    				<button type="button" class="hasDropdown">
                        <span class="dropdownLabel">Average knowledge</span>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </button>
    				<div class="selectGroup">
                        @foreach($technologies as $technology)
                            <label>
                                <input name="average_knowledge[]" type="checkbox" value="{{ $technology->id }}"
                                @for($i = 0; $i < count($profile->average_knowledge); $i++ )
                                    {{ ($technology->id == $profile->average_knowledge[$i]) ? 'checked="checked"' : '' }}
                                @endfor>
                                <span>{{ $technology->name }}</span>
                            </label>
                        @endforeach
                    </div>
    			</div>
			</div>
            <div class="work-experience">
                <h5>Work experience</h5>
                @foreach($user->work_experiences as $work_experience)
                    <div class="work-experience-container">
                        <button class="btn btn-danger btn_remove"><i class="fa fa-times" aria-hidden="true"></i></button>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company">Company</label>
                                <input type="text" class="form-control" name="company[]" value="{{ $work_experience->company }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="occupational_title">Occupational title</label>
                                <input type="text" class="form-control" name="occupational_title[]" value="{{ $work_experience->occupational_title }}">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="description">Description</label>
                            <textarea class="form-control" rows="3" name="description[]">{{ $work_experience->description }}</textarea>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="date_from">Period from</label>
                                <input class="form-control datepicker" type="text" name="date_from[]" value="{{ $work_experience->period_from }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="date_to">Period to</label>
                                <input class="form-control datepicker" type="text" name="date_to[]" value="{{ $work_experience->period_to }}">
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <button id="add" class="btn btn-primary">Add more</button>
			<button type="submit" class="btn btn-orange">Edit profile</button>

		</form>	
    </div>
</div>

@endsection