@extends('layouts.backend')

@section('title', 'Profil erstellen')

@section('content')

<div id="form-container-dark" class="container">
    <div class="container col-xl-8 col-lg-8 col-md-10">
        <a href="{{ URL::route('dashboard') }}" class="btn btn-primary backend-back-button">back to the dashboard</a>
		<h1>Create profile</h1>
        
		<form method="POST" action="{{route('profile.store')}}" enctype="multipart/form-data">
        
			{{ csrf_field() }}
            
            @include('layouts.errors')
            
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                </div>
                <div class="form-group col-md-6">
                    <label for="surname">Surname</label>
                    <input type="text" class="form-control" name="surname" value="{{ old('surname') }}">
                </div>
            </div>
            <div class="row">
    		  	<div class="form-group col-md-5">
    			    <label for="state">State</label>
    			    <select name="state" class="custom-select">
    			    	<option value="" selected disabled>Please choose ...</option>
                        @foreach($states as $state)
                            <option value="{{$state->id}}" {{ ($state->id == old('state')) ? 'selected="selected"' : "" }}>
                                {{ ucfirst($state->name) }}
                            </option>
                        @endforeach
    			    </select>
    		  	</div>
                <div class="form-group col-md-2">
                    <label for="age">Age</label>
                    <input type="number" class="form-control" name="age" value="{{ old('age') }}">
                </div>
                <div class="form-group col-md-5">
                    <input type="file" name="curriculum_vitae" id="file" class="inputfile">
                    <label for="file" class="col-md-12 file-upload">
                        <i class="fa fa-upload" aria-hidden="true"></i>
                        upload curriculum vitae
                    </label>
               </div>
            </div>
			<div class="form-group col-md-12">
				<label for="about_me">About me</label>
		    	<textarea class="form-control" rows="3" name="about_me">{{ old('about_me') }}</textarea>
			</div>
            <div class="row">
    			<div class="form-group col-md-6 dropdownContainer">
    				<button type="button" class="hasDropdown">
                        <span class="dropdownLabel">Good knowledge</span>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </button>
    				<div class="selectGroup">
                        @foreach($technologies as $technology)
                            <label>
                                <input name="good_knowledge[]" type="checkbox" value="{{ $technology->id }}">
                                <span>{{ $technology->name }}</span>
                            </label>
                        @endforeach
                    </div>
    			</div>
    			<div class="form-group col-md-6 dropdownContainer">
    				<button type="button" class="hasDropdown">
                        <span class="dropdownLabel">Average knowledge</span>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </button>
    				<div class="selectGroup">
                        @foreach($technologies as $technology)
                            <label>
                                <input name="average_knowledge[]" type="checkbox" value="{{ $technology->id }}">
                                <span>{{ $technology->name }}</span>
                            </label>
                        @endforeach
                    </div>
    			</div>
			</div>
            <div class="work-experience">
                <h5>Work experience</h5>
                <div class="work-experience-container">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="company">Company</label>
                            <input type="text" class="form-control" name="company[]">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="occupational_title">Occupational title</label>
                            <input type="text" class="form-control" name="occupational_title[]">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="description">Description</label>
                        <textarea class="form-control" rows="3" name="description[]">{{ old('description') }}</textarea>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="date_from">Period from</label>
                            <input class="form-control datepicker" type="text" name="date_from[]">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="date_to">Period to</label>
                            <input class="form-control datepicker" type="text" name="date_to[]">
                        </div>
                    </div>
                </div>
            </div>
            <button id="add" class="btn btn-primary">Add more</button>
			<button type="submit" class="btn btn-orange">Create profile</button>

		</form>	
    </div>
</div>

@endsection