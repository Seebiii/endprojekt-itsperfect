@extends('layouts.backend')

@section('title', 'Dein Profil')

@section('content')

	<div class="container col-xl-8 col-lg-8 col-md-8">

		@if(isset($profile))

			<div class="modal">
				<div class="modal-dialog col-md-8 col-xs-12" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">curriculum vitae</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<object data="{{ asset('storage/curriculum_vitae/' . $profile->curriculum_vitae) }}" type="application/pdf">
							  	<embed src="{{ asset('storage/curriculum_vitae/' . $profile->curriculum_vitae) }}" type='application/pdf'>
							</object>
						</div>
					</div>
				</div>
			</div>
			
			<h1>Profile overview</h1>
			<a href="{{ action('ProfileController@edit', $profile) }}" class="btn btn-primary">edit profile</a>
			<form class="delete" action="{{ action('ProfileController@destroy', $profile) }}" method="POST" data-confirm="Are you sure to delete your profile?">
				{{ method_field('DELETE') }}
				{{ csrf_field() }}
				<button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> delete profile</button>
			</form>
			<table class="col-md-12 table-striped">
				<tbody>
					<tr>
						<th>Name:</th>
						<td>{{$profile->name}}</td>
					</tr>
					<tr>
						<th>Surname:</th>
						<td>{{$profile->surname}}</td>
					</tr>
					<tr>
						<th>Age:</th>
						<td>{{$profile->age}}</td>
					</tr>
					<tr>
						<th>About me:</th>
						<td>{!! $profile->about_me !!}</td>
					</tr>
					<tr>
						<th>Meine Programmiersprachen:</th>
						<td>
							<ul>
								@foreach($profile->technologies as $technology)
								<li>{{ $technology->name }}</li>
								@endforeach
							</ul>
						</td>
					</tr>
					<tr>
						<th>Curriculum vitae:</th>
						<td>
							<button id="show-pdf" name="curriculum_vitae" class="btn btn-orange">
							Show curriculum vitae
							</button>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="col-md-12 table-striped">
				<thead>
					<tr>
						<td>Company</td>
						<td>Occupational title</td>
						<td>Description</td>
						<td>Period from</td>
						<td>Period to</td>
					</tr>
				</thead>
				<tbody>
					@foreach($workExperience as $experience)
					<tr>
						<td>{{ $experience->company }}</td>
						<td>{{ $experience->occupational_title }}</td>
						<td>{!! $experience->description !!}</td>
						<td>{{ $experience->period_from }}</td>
						<td>{{ $experience->period_to }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			
 		@else
			<h1>Profil overview</h1>
			<p>Um den vollen Funktionsumfang der Website nutzen zu können empfehlen wir ein Profil zu erstellen!</p>
			<a class="btn btn-orange" href="{{ route('profile.create') }}">Profil erstellen</a>
 		@endif
	</div>

@endsection