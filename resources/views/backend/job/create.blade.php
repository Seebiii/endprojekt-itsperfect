@extends('layouts.backend')

@section('title', 'Job erstellen')

@section('content')

<div id="form-container-dark" class="container">
    <div class="container col-xl-8 col-lg-8 col-md-10">

        <a href="{{ URL::route('job.index') }}" class="btn btn-primary backend-back-button">back to jobs overview</a>
		<h1>Create job</h1>
        
		<form method="POST" action="{{ route('job.index') }}">
        
			{{ csrf_field() }}
            
            @include('layouts.errors')
            
            <div class="form-group col-md-12">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" value="{{ old('title') }}">
            </div>
            <div class="row">
    		  	<div class="form-group col-md-6">
    			    <label for="state">State</label>
    			    <select name="state" class="custom-select">
    			    	<option value="" selected disabled>Please choose ...</option>
                        @foreach($states as $state)
                            <option value="{{$state->id}}" {{ ($state->id == old('state')) ? 'selected="selected"' : "" }}>
                                {{ ucfirst($state->name) }}
                            </option>
                        @endforeach
    			    </select>
    		  	</div>
                <div class="form-group col-md-6">
                    <label for="employment">Employment type</label>
                    <select name="employment" class="custom-select">
                        <option value="" selected disabled>Please choose ...</option>
                        @foreach($employments as $employment)
                            <option value="{{$employment->id}}" {{ ($employment->id == old('employment')) ? 'selected="selected"' : "" }}>
                                {{ ucfirst($employment->name) }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
			<div class="form-group col-md-12">
				<label for="description">Description</label>
		    	<textarea class="form-control" rows="3" name="description">{{ old('description') }}</textarea>
			</div>
            <div class="row">
    			<div class="form-group col-md-6 dropdownContainer">
    				<button type="button" class="hasDropdown">
                        <span class="dropdownLabel">Required knowledge</span>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </button>
    				<div class="selectGroup">
                        @foreach($technologies as $technology)
                            <label>
                                <input name="required_knowledge[]" type="checkbox" value="{{ $technology->id }}">
                                <span>{{ $technology->name }}</span>
                            </label>
                        @endforeach
                    </div>
    			</div>
    			<div class="form-group col-md-6 dropdownContainer">
    				<button type="button" class="hasDropdown">
                        <span class="dropdownLabel">Desired knowledge</span>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </button>
    				<div class="selectGroup">
                        @foreach($technologies as $technology)
                            <label>
                                <input name="desired_knowledge[]" type="checkbox" value="{{ $technology->id }}">
                                <span>{{ $technology->name }}</span>
                            </label>
                        @endforeach
                    </div>
    			</div>
			</div>
			<button type="submit" class="btn btn-orange">Create job</button>

		</form>
    </div>
</div>

@endsection