@extends('layouts.backend')

@section('title', 'Jobs')

@section('content')

@include('includes.messages')

<div class="container">

	<div class="col-xl-12">
	<h1>Jobs overview</h1>
	<a href="{{ URL::route('job.create') }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Post new job</a>

	<table class="table">
		<thead class="thead-default">
			<th>Title</th>
			<th colspan="3">Actions</th>
			<th>Created at</th>
		</thead>
		<tbody>
			@forelse($jobs as $job)
			  <tr class="{{ ($job->is_active === 0) ? "bg-danger" : "" }}">
		          <td>{{ $job->title }}</td>
		          <td><a href="{{ action('JobController@edit', $job) }}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i> edit</a></td>
		          <td>
		          	<a href="{{ URL::route('job.isActive', $job) }}" class="btn btn-warning">
			          	@if($job->is_active === 1)
			          		<i class="fa fa-eye-slash" aria-hidden="true"></i> deactivate
			          	@else
							<i class="fa fa-eye" aria-hidden="true"></i> activate
			          	@endif
	          		</a>
	          	</td>
	          	{{-- <td>
					<a href="{{ URL::route('job.isTopJob', $job) }}" class="btn btn-warning">
			          	@if($job->is_top_job === 1)
			          		<i class="fa fa-eye-slash" aria-hidden="true"></i> deactivate TopJob
			          	@else
							<i class="fa fa-eye" aria-hidden="true"></i> activate TopJob
			          	@endif
	          		</a>
				</td> --}}
		          <td>
					<form class="delete" action="{{ action('JobController@destroy', $job) }}" method="POST" data-confirm="Stelleninserat wirklich löschen?">
						{{ method_field('DELETE') }}
						{{ csrf_field() }}
						<button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> delete</button>
					</form>
				</td>
				<td>{{ $job->created_at->format('d.m.Y')}}</td>
			@empty
				<td colspan="5">Unfortunately no jobs were found! You can create a new one <a href="{{ URL::route('job.create')}}">here</a></td>
			  </tr>
			@endforelse
		</tbody>
	</table>
	</div>
</div>

@endsection