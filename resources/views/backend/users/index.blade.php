@extends('layouts.backend')

@section('title', 'Users')

@section('content')

	<table class="table table-striped">
		<thead class="thead-default">
			<th>E-Mail</th>
			<th>Employer</th>
			<th>Employee</th>
			<th>Admin</th>
			<th></th>
		</thead>
		<tbody>
		@foreach($users as $user)
		  <tr>
		      <form action="{{ route('admin.assign') }}" method="post">
		          <td>{{ $user->email }} <input type="hidden" name="email" value="{{ $user->email }}"></td>
		          <td><input type="checkbox" {{ $user->hasAnyRole('employer') ? 'checked' : '' }} name="role_employer"></td>
		          <td><input type="checkbox" {{ $user->hasAnyRole('employee') ? 'checked' : '' }} name="role_employee"></td>
		          <td><input type="checkbox" {{ $user->hasAnyRole('admin') ? 'checked' : '' }} name="role_admin"></td>
		          {{ csrf_field() }}
		          <td><button type="submit">Assign roles</button></td>
		      </form>
		  </tr>
		@endforeach
		</tbody>
	</table>

@endsection