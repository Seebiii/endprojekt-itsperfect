@extends('layouts.app')

@section('title', 'Home')
@section('body_id', 'home')

@section('content')

<div id="hero-img">
	<div class="container">

		<h3>19.112 job offers in austria</h3>
		<h2>Get employed faster</h2>
        
		<form id="job-search-form" action="{{ route('search') }}" method="GET">
            {{-- {{ csrf_field() }} --}}
            
			<div class="form-group">
		    	<input type="text" class="form-control col-md-12" id="search-input" name="keyword" placeholder="Search for jobs ...">
	  		</div>

	  		<div class="form-group row">
				<div class="dropdownContainer col-md-4 col-sm-12">
                	<button type="button" class="hasDropdown">
                    	<span class="dropdownLabel">Technology</span>
                    	<i class="fa fa-angle-down" aria-hidden="true"></i>
                	</button>
                	<div class="selectGroup">
                        @foreach($technologies as $technology)
                            <label>
                                <input name="technology[]" type="checkbox" value="{{ $technology->id }}">
                                <span>{{ $technology->name }}</span>
                            </label>
                        @endforeach
                	</div>
               	</div>
				<div class="dropdownContainer col-md-4 col-sm-12">
                	<button type="button" class="hasDropdown">
                    	<span class="dropdownLabel">Employment</span>
                    	<i class="fa fa-angle-down" aria-hidden="true"></i>
                	</button>
                	<div class="selectGroup">
                        @foreach($employment as $employment)
                            <label>
                                <input name="employment[]" type="checkbox" value="{{ $employment->id }}">
                                <span>{{ $employment->name }}</span>
                            </label>
                        @endforeach
                	</div>
               	</div>
				<div class="dropdownContainer col-md-4 col-sm-12">
                	<button type="button" class="hasDropdown">
                    	<span class="dropdownLabel">State</span>
                    	<i class="fa fa-angle-down" aria-hidden="true"></i>
                	</button>
                	<div class="selectGroup">
                        @foreach($states as $state)
                            <label>
                                <input name="state[]" type="checkbox" value="{{ $state->id }}">
                                <span>{{ $state->name }}</span>
                            </label>
                        @endforeach
                	</div>
               	</div>
			</div>
			<div class="form-group col-xs-12">
            	<button class="btn btn-orange" type="submit">Search</button>
        	</div>
		</form>

	</div>
</div>

@stop