@section('jobs.index')

<div id="jobs-container">
    <div class="container">
        <div class="row search-area">
            <div class="col-lg-12 col-md-12">
                <h2>
                    @if($jobs->count() === 1) 
                        {{$jobs->count() . ' job was found' }} 
                    @else
                        {{$jobs->count() . ' jobs were found' }}
                    @endif
                </h2>
            </div>
        </div>
        <div class="row">
            <ul class="col-lg-12 col-md-12 job-item-container">
                @forelse($jobs as $job)
                    @if($job->is_active == 1)
                        <li class="job-item">
                            <a href="/jobs/{{ $job->id }}" class="title" target="_blank">{{ $job->title }}</a>
                            <br>
                            <p>
                                <span class="company">{{ $job->user->company_name }}</span>
                                &middot;
                                <span>{{ $job->employment->name }}</span>
                                &middot;
                                <span class="country">{{ $job->state->name }}</span>
                                on
                                <span class="date">{{ $job->created_at->format('jS \o\f F Y') }}</span>
                            </p>
                            <div>{!! str_limit($job->description, 150) !!}</div>
                            <p class="tags">
                                @foreach($job->technologies as $technology)
                                    <span>{{$technology->name}}</span>
                                @endforeach
                            </p>
                            @if(Auth::check() && Auth::user()->isRole('employee') && $profile != null)
                                <div class="match-result">
                                    <span class="match-background"></span>
                                    <div class="match-text">
                                        <h4>{{ $job->calculation }} %</h4>
                                        <p>matching</p>
                                    </div>
                                </div>
                            @endif
                        </li>
                    @endif
                @empty
                    <div class="alert alert-danger job-error">
                        <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                        No results found, please try with different keywords.
                    </div>
                @endforelse
                <br>
                {{ $jobs->links() }}
            </ul>
        </div>
    </div>
</div>