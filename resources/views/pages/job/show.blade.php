@extends('layouts.app')

@section('title', 'View job')
@section('body_id', 'job-single')

@section('content')

@include('includes.messages')

@if($job->status !== 0)
	<div class="job-single-nav">
		<div class="container">
			<a class="btn btn-primary" href="{{ route('search') }}">back to jobs search</a>
		</div>
	</div>

	<div class="job-single-content">
		<div class="container">

			<div class="col-xs-12">
				<div class="heading">
					<h4 class="col-lg-4 col-md-12">{{ $job->title }}</h4>
					<div class="info col-lg-8 col-md-12">
						<h5>
							<i class="fa fa-clock-o" aria-hidden="true"></i>
							{{ $job->employment->name }}
						</h5>
						<h5>
							<i class="fa fa-globe" aria-hidden="true"></i>
							{{ $job->state->name }}
						</h5>
						<h5>
							<i class="fa fa-calendar" aria-hidden="true"></i>
							{{ $job->created_at->format('d.m.Y') }}</h5>
						<h5>
							<i class="fa fa-building" aria-hidden="true"></i>
							{{ $job->user->company_name }}
						</h5>
					</div>
				</div>
				<div class="description">
					{!! $job->description !!}
				</div>
				
				@if(Auth::check() && Auth::user()->isRole('employee') && $profile != null)
					<button id="btn-apply-now" class="btn btn-orange col-md-8 offset-md-2 col-xs-12">Apply now</button>
				@endif

			</div>
		</div>
	</div>
	
	@if(Auth::check() && Auth::user()->isRole('employee') && $profile != null)

		<div class="modal">
			<div class="modal-dialog col-md-8 col-xs-12" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">curriculum vitae</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<object data="{{ asset('storage/curriculum_vitae/' . $profile->curriculum_vitae) }}" type="application/pdf">
						  	<embed src="{{ asset('storage/curriculum_vitae/' . $profile->curriculum_vitae) }}" type='application/pdf'>
						</object>
					</div>
				</div>
			</div>
		</div>

		<div id="form-container-dark">
			<div class="container">
				<form action="{{ route('job.send', $job) }}" method="POST" enctype="multipart/form-data">

					{{ csrf_field() }}

					<div class="form-group col-md-8 offset-md-2">
						@include('layouts.errors')
					</div>

					<div class="row">
						<div class="form-group col-md-4 offset-md-2 offset-md-2">
							<label for="name">Name</label>
			                   	<input type="text" class="form-control" name="name" value="{{ $profile->name }}">
			               </div>
			               <div class="form-group col-md-4">
			               	<label for="surname">Surname</label>
			                   <input type="text" class="form-control" name="surname" value="{{ $profile->surname }}">
			               </div>
			          </div>
			          <div class="row">
						<div class="form-group col-md-4 offset-md-2">
							<label for="email">Email</label>
			                   	<input type="text" class="form-control" name="email" value="{{ Auth::user()->email }}">
			               </div>
			               <div class="form-group col-md-4">
			               	@if(!empty($profile->curriculum_vitae))
								<p>Curriculum vitae</p>
								<div class="curriculum_vitae">
									<button id="show-pdf" name="curriculum_vitae" class="col-md-12 btn btn-curriculum-vitae">
										show
									</button>
								</div>
							@endif
			               </div>
			          </div>
					<div class="row">
						<div class="form-group col-md-4 offset-md-2">
							<label for="state">State</label>
							<select name="state" class="custom-select">
								<option value="" disabled>Please choose ...</option>
								@foreach($states as $state)
									<option value="{{$state->name}}" {{ ($state->id == $profile->state_id) ? 'selected="selected"' : "" }}>
										{{ ucfirst($state->name) }}
									</option>
								@endforeach
							</select>
						</div>
						<div class="form-group col-md-4">
							<label for="age">Age</label>
							<input type="number" class="form-control" name="age" value="{{ $profile->age }}">
						</div>
               		</div>
               		<div class="row">
               			<div class="form-group col-md-4 offset-md-2 dropdownContainer">
			    				<button type="button" class="hasDropdown">
			                        <span class="dropdownLabel">Good knowledge</span>
			                        <i class="fa fa-angle-down" aria-hidden="true"></i>
			                    </button>
			    				<div class="selectGroup">
			                        @foreach($technologies as $technology)
			                            <label>
			                                <input name="good_knowledge[]" type="checkbox" value="{{ $technology->name }}" 
			                                @for($i = 0; $i < count($profile->good_knowledge); $i++ )
			                                    {{ ($technology->id == $profile->good_knowledge[$i]) ? 'checked="checked"' : '' }}
			                                @endfor>
			                                <span>{{ $technology->name }}</span>
			                            </label>
			                        @endforeach
			                    </div>
    						</div>
    						<div class="form-group col-md-4 dropdownContainer">
			    				<button type="button" class="hasDropdown">
			                        <span class="dropdownLabel">Average knowledge</span>
			                        <i class="fa fa-angle-down" aria-hidden="true"></i>
			                    </button>
			    				<div class="selectGroup">
			                        @foreach($technologies as $technology)
			                            <label>
			                                <input name="average_knowledge[]" type="checkbox" value="{{ $technology->name }}" 
			                                @for($i = 0; $i < count($profile->average_knowledge); $i++ )
			                                    {{ ($technology->id == $profile->average_knowledge[$i]) ? 'checked="checked"' : '' }}
			                                @endfor>
			                                <span>{{ $technology->name }}</span>
			                            </label>
			                        @endforeach
			                    </div>
    						</div>
               		</div>
			          <div class="form-group col-md-8 offset-md-2">
			              <textarea name="msg" cols="30" rows="10" placeholder="Deine Nachricht!">
							<p>Sehr geehrte Damen und Herren!</p>
							<p>Engagement, Teamfähigkeit und Ehrgeiz sind meine großen Stärken. Die von Ihnen ausgeschriebene Stelle entspricht darüber hinaus auch sehr gut meinem Fachwissen.</p>
							<p>Ich freue mich auf ein persönliches Kennenlernen!</p>
							<p>Freundliche Grüße,</p>
							<p>(Vorname) (Nachname)</p>
			              </textarea>
			          </div>

		               <div class="form-group col-md-6 offset-md-3 col-xs-12">
		                   <button class="btn btn-orange col-md-8 offset-md-2 col-xs-12">Send job application</button>
		               </div>
				</form>
			</div>
		</div>
	@endif
@endif


@stop