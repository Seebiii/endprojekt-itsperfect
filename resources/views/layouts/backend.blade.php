<!DOCTYPE html>
<html lang="en">
<head>	
	@include('includes.head')
</head>
<body id="backend">

	<main>
		@include('includes.sidebar')
		@yield('content')
	</main>
	
	@include('includes.footer')
	
</body>
</html>