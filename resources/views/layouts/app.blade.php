<!DOCTYPE html>
<html lang="en">
<head>	
	@include('includes.head')
</head>
<body id="@yield('body_id')">

	@include('includes.menu')

	<main>
		@yield('content')
		
		@if(Request::url() === URL::route('search'))
			@include('pages.job.index')
		@endif
	</main>
	
	@include('includes.footer')
	
</body>
</html>