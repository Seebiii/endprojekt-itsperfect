@extends('layouts.app')

@section('title', 'Reset password')
@section('body_id', 'form_light')

@section('content')

<div id="form-container-light" class="reset">
    <div id="circle-background-auth"></div>
    <div class="container">
        
        <div id="form-box" class="col-md-5">
        
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form id="pw-reset-form" class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">
                {{ csrf_field() }}

                <h5>Reset password</h5>

                <input type="hidden" name="token" value="{{ $token }}">

                @include('layouts.errors')

                <div class="col-md-10 form-group">
                    <div class="col-xs-12">
                        <input id="email" type="email" class="form-control" name="email" placeholder="Email address (required)" value="{{ $email or old('email') }}" required autofocus>
                    </div>
                </div>

                <div class="col-md-10 form-group">
                    <div class="col-xs-12">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Password (required)" required>
                    </div>
                </div>

                <div class="col-md-10 form-group">
                    <div class="col-xs-12">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password (required)" required>
                    </div>
                </div>

                <div class="col-md-10 form-group">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-orange">
                            Reset password
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
