@extends('layouts.app')

@section('title', 'Reset password')
@section('body_id', 'form_light')

@section('content')

<div id="form-container-light" class="reset">
    <div id="circle-background-auth"></div>
    <div class="container">

            <div id="form-box" class="col-md-5">
                    
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form id="pw-reset-form" class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                     <h5>Reset password</h5>

                    <div class="col-md-10 form-group">
                        <div class="col-xs-12">
                            <input id="email" type="email" class="form-control" name="email" placeholder="Email address (required)" value="{{ old('email') }}" required>
                        </div>
                    </div>

                    <div class="col-md-10 form-group">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-orange">
                                Reset password
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
