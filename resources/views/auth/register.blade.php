@extends('layouts.app')

@section('title', 'Registrieren')
@section('body_id', 'form_light')

@section('content')

<div id="form-container-light">
    <div id="circle-background-auth"></div>
    <div class="container">
        <div id="form-box" class="col-xl-5 col-lg-8 col-md-10">
            <div class="form-head">
                <p>Are you already registered? <a href="{{ route('login') }}">Login now!</a></p>
            </div>
            <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <h5>Register</h5>

                <div class="col-md-10 form-group">
                    @include('layouts.errors')
                </div>

                <div class="col-md-10 form-group">
                    <div class="col-xs-12">
                        <input id="email" type="email" class="form-control" placeholder="Email adress (required)" name="email" value="{{ old('email') }}" required>
                    </div>
                </div>

                <div class="col-md-10 form-group">
                    <div class="col-xs-12">
                        <input id="password" type="password" class="form-control" placeholder="Password (required)" name="password" required>
                    </div>
                </div>

                <div class="col-md-10 form-group">
                    <div class="col-xs-12">
                        <input id="password-confirm" type="password" class="form-control" placeholder="Password confirm (required)" name="password_confirmation" required>
                    </div>
                </div>

                <div class="col-md-10 form-group">
                    <select class="form-control custom-select" id="role" name="role" required>
                        <option disabled selected>Register as ...</option>
                        <option value="employee">Employee</option>
                        <option value="employer">Employer</option>
                    </select>
                </div>

                <div class="col-md-10 form-group">
                    <div class="col-xs-12 col-md-offset-4">
                        <button class="btn btn-orange" type="submit" class="btn btn-primary">
                            Complete registration
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
