@extends('layouts.app')

@section('title', 'Anmelden')
@section('body_id', 'form_light')

@section('content')

<div id="form-container-light">
    <div id="circle-background-auth"></div>
    <div class="container">
        <div id="form-box" class="col-xl-5 col-lg-8 col-md-10">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                
                <h5>Login</h5>

                <div class="form-group col-md-8 offset-md-2">
                    @include('layouts.errors')
                </div>

                <div class="col-md-10 form-group">
                    <div class="col-xs-12">
                        <input id="email" type="email" class="form-control" name="email" placeholder="Email adress" value="{{ old('email') }}" autofocus required>
                    </div>
                </div>

                <div class="col-md-10 form-group">
                    <div class="col-xs-12">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                </div>

                <div class="col-md-10 form-group">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-orange">
                            Sign in
                        </button>
                        
                        <div class="row">
                            <label class="custom-control custom-checkbox col-6">
                                <input type="checkbox" class="custom-control-input" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Remember me</span>
                            </label>

                            <a class="btn btn-link col-6" href="{{ route('password.request') }}">
                                Forgot Password?
                            </a>
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-footer">
                <p>Don't have an account? <a href="{{ route('register') }}">Get started!</a></p>
            </div>
        </div>
    </div>
</div>

@endsection