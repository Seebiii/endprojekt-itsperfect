/*****************************
*
* NAV TOGGLE
*
*****************************/
$('.navbar-toggler, .navbar-toggler-close').click(function() {
    	$('body').toggleClass('overlay');
});


/*****************************
*
* JOB SEARCH FORM - TOGGLE
*
*****************************/
$('.selectGroup').each(function() {
	$(this).hide();
});
$('.hasDropdown').click(function() {
	$(this).next().slideToggle();
});


/*****************************
*
* APPLY NOW - TOGGLE
*
*****************************/
$('#btn-apply-now').on('click', function() {
	// Form toggle
	$('#form-container-dark').toggle(100 ,'swing');
	// Footer bg color toggle
	// $('footer').toggleClass('footer-dark');
});


/*****************************
*
* NAV SET ACTIVE CLASS
*
*****************************/
var url = window.location;
// Will only work if string in href matches with location
$('ul.nav a[href="'+ url +'"]').parent().addClass('active');

// Will also work for relative and absolute hrefs
$('ul.nav a').filter(function() {
    return this.href == url;
}).parent().addClass('active');



/*****************************
*
* DELETE CONFIRMATION
*
*****************************/
$(document).ready(function () {

	$("form[data-confirm]").on('click', function (e) {

		e.preventDefault();

		var form = $(this).closest('form');
		var msg = form.data('confirm');

		if(confirm(msg)) {
			form.submit();
		};
	});
});



/*****************************
*
* JOB EXPERIENCE FORM
*
*****************************/
$(document).ready(function() {
	var i = 1;

	$('#add').click(function(event) {
		event.preventDefault();
		i++;
		$('.work-experience').append(
      		'<div id="row'+i+'" class="work-experience-container">' +
      			'<button class="btn btn-danger btn_remove"><i class="fa fa-times" aria-hidden="true"></i></button>' +
          		'<div class="row">' +
              			'<div class="form-group col-md-6">' +
                  			'<label for="company">Company</label>' +
                  			'<input type="text" class="form-control" name="company[]">' +
             			'</div>' +
                   		'<div class="form-group col-md-6">' +
                       		'<label for="occupational_title">Occupational title</label>' +
                       		'<input type="text" class="form-control" name="occupational_title[]">' +
                   		'</div>' +
          		'</div>' +
               	'<div class="form-group col-md-12">' +
                   		'<label for="description">Description</label>' +
                   		'<textarea class="form-control" rows="3" name="description[]"></textarea>' +
               	'</div>' +
               	'<div class="row">' +
                   		'<div class="form-group col-md-6">'+
                       		'<label for="date_from">Period from</label>' +
                       		'<input class="form-control datepicker" type="text" name="date_from[]">' +
                   		'</div>' +
              			'<div class="form-group col-md-6">' +
                  			'<label for="date_to">Period to</label>' +
                  			'<input class="form-control datepicker" type="text" name="date_to[]">' +
              			'</div>' +
          		'</div>' +
      		'</div>'
		);

		// remove work-experience-container button
		$(document).on('click', '.btn_remove', function(e) {
			e.preventDefault();
			if(confirm('Are you sure you want to remove this?')) {
				$(this).parent().remove();
			}
		});

		// init tinymce editor for dynamic created form data
		tinymce.init({ 
			selector: 'textarea',
			menubar: false,
			toolbar: 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
			content_style: ".mce-content-body {background: #101723; color: #fff;}"
	   	});

		// init datepicker for dynamic created form data
	 	$('.datepicker').datepicker({
			 format: {
		        toDisplay: function (date, format, language) {
		            var date = new Date(date),
		            month = '' + (date.getMonth() + 1),
		            day = '' + date.getDate(),
		            year = date.getFullYear();

		            if (month.length < 2) month = '0' + month;
		            if (day.length < 2) day = '0' + day;
		            return [day, month, year].join('/');
		        },
		        toValue: function (date, format, language) {
		            var date = new Date(date),
		            month = '' + (date.getMonth() + 1),
		            day = '' + date.getDate(),
		            year = date.getFullYear();

		            if (month.length < 2) month = '0' + month;
		            if (day.length < 2) day = '0' + day;

		            return [year, month, day].join('-');
		        }
		    }
		});

		// store data into browser storage
		// sessionStorage.workExperiences = $('.work-experience').html();

		// console.log(sessionStorage.workExperiences);
	});

	$('.btn_remove').click(function(e) {
		e.preventDefault();
		if(confirm('Are you sure you want to remove this?')) {
			$(this).parent().remove();
		}
	});

	// $(window).on('load', function() {
	// 	if(sessionStorage.workExperiences) {
	// 		$('.work-experience').empty().append(sessionStorage.workExperiences);
	// 	}
	// });
});



/*****************************
*
* apply job
*
*****************************/

/* ----------------------
	show pdf
*/
$('#show-pdf').click(function(e) {
	e.preventDefault();

	$('.modal').addClass('active');
});

/* ----------------------
	dismiss modal
*/
$('[data-dismiss="modal"]').click(function(e) {
	e.preventDefault();

	$('.modal').removeClass('active');
})



/*****************************
*
* employer register company name
*
*****************************/
$(document).ready(function() {
	var div = '<div id="company_input" class="col-md-10 form-group">' +
                    '<div class="col-xs-12">' +
                        '<input type="text" class="form-control" name="company_name" placeholder="Company name" required>' +
                    '</div></div>';

	$('#role').on('change', function(){
		if( $(this).val() === "employer") {
			console.log($('.form-group:nth-last-of-type(2)').after(div));
		} else if( $(this).val() === "employee") {
			$('#company_input').remove();
		}
	});
});
