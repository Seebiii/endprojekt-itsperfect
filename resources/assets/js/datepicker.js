require('bootstrap-datepicker');

$('.datepicker').datepicker({
	 format: {
        toDisplay: function (date, format, language) {
            var date = new Date(date),
            month = '' + (date.getMonth() + 1),
            day = '' + date.getDate(),
            year = date.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            return [day, month, year].join('/');
        },
        toValue: function (date, format, language) {
            var date = new Date(date),
            month = '' + (date.getMonth() + 1),
            day = '' + date.getDate(),
            year = date.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }
    }
});