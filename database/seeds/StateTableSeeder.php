<?php

use App\State;
use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $state_wien = new State();
        $state_wien->name = 'vienna';
        $state_wien->save();

        $state_burgenland = new State();
        $state_burgenland->name = 'burgenland';
        $state_burgenland->save();

        $state_niederoestereich = new State();
        $state_niederoestereich->name = 'lower austria';
        $state_niederoestereich->save();

        $state_oberoesterreich = new State();
        $state_oberoesterreich->name = 'upper austria';
        $state_oberoesterreich->save();

        $state_kaernten = new State();
        $state_kaernten->name = 'carinthia';
        $state_kaernten->save();

        $state_salzburg = new State();
        $state_salzburg->name = 'salzburg';
        $state_salzburg->save();

        $state_steiermark = new State();
        $state_steiermark->name = 'styria';
        $state_steiermark->save();

        $state_tirol = new State();
        $state_tirol->name = 'tyrol';
        $state_tirol->save();

        $state_vorarlberg = new State();
        $state_vorarlberg->name = 'vorarlberg';
        $state_vorarlberg->save();
    }
}
