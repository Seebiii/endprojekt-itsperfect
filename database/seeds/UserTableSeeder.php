<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employer = Role::where('name', 'employer')->first();
        $role_employee = Role::where('name', 'employee')->first();
        $role_admin = Role::where('name', 'admin')->first();

        $employer = new User();
        $employer->email = 'employer@employer.com';
        $employer->password = bcrypt('employer');
        $employer->company_name = 'Firmenname GmbH';
        $employer->save();
        $employer->roles()->attach($role_employer);

        $employee = new User();
        $employee->email = 'employee@employee.com';
        $employee->password = bcrypt('employee');
        $employee->save();
        $employee->roles()->attach($role_employee);

        $admin = new User();
        $admin->email = 'admin@admin.com';
        $admin->password = bcrypt('admin');
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
