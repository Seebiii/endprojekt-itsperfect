<?php

use App\Technology;
use Illuminate\Database\Seeder;

class TechnologyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$programmingLanguage_php = new Technology();
		$programmingLanguage_php->name = 'PHP';
		$programmingLanguage_php->save();

		$programmingLanguage_c_plus_plus = new Technology();
		$programmingLanguage_c_plus_plus->name = 'C++';
		$programmingLanguage_c_plus_plus->save();

		$programmingLanguage_javascript = new Technology();
		$programmingLanguage_javascript->name = 'JavaScript';
		$programmingLanguage_javascript->save();

		$programmingLanguage_phyton = new Technology();
		$programmingLanguage_phyton->name = 'Phyton';
		$programmingLanguage_phyton->save();

		$programmingLanguage_r = new Technology();
		$programmingLanguage_r->name = 'R';
		$programmingLanguage_r->save();

		$programmingLanguage_perl = new Technology();
		$programmingLanguage_perl->name = 'Perl';
		$programmingLanguage_perl->save();

		$programmingLanguage_ruby = new Technology();
		$programmingLanguage_ruby->name = 'Ruby';
		$programmingLanguage_ruby->save();

		$programmingLanguage_swift = new Technology();
		$programmingLanguage_swift->name = 'Swift';
		$programmingLanguage_swift->save();

		$programmingLanguage_java = new Technology();
		$programmingLanguage_java->name = 'Java';
		$programmingLanguage_java->save();
    }
}
