<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employer = new Role();
        $role_employer->name = 'employer';
        $role_employer->save();

        $role_employee = new Role();
        $role_employee->name = 'employee';
        $role_employee->save();

        $role_admin = new Role();
        $role_admin->name = 'admin';
        $role_admin->save();
    }
}
