<?php

use App\Employment;
use Illuminate\Database\Seeder;

class EmploymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employment_full_time = new Employment();
        $employment_full_time->name = 'full time';
        $employment_full_time->save();

        $employment_part_time = new Employment();
        $employment_part_time->name = 'part time';
        $employment_part_time->save();

        $employment_minimally_employed = new Employment();
        $employment_minimally_employed->name = 'minimally employed';
        $employment_minimally_employed->save();

        $employment_internship = new Employment();
        $employment_internship->name = 'internship';
        $employment_internship->save();

        $employment_project_based = new Employment();
        $employment_project_based->name = 'project based';
        $employment_project_based->save();
    }
}
