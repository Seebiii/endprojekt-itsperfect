<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechnologiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technologies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });
        

        Schema::create('job_technology', function (Blueprint $table) {
            $table->increments('id'); // until duplicate tags is fixed between desired and required knowledge
            $table->integer('technology_id');
            $table->integer('job_id');
        });

        Schema::create('profile_technology', function (Blueprint $table) {
            $table->increments('id'); // until duplicate tags is fixed between good and average knowledge
            $table->integer('technology_id');
            $table->integer('profile_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
        Schema::dropIfExists('job_tag');
    }
}
