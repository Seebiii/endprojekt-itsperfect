<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'company_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the jobs a user has
     */
    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    /**
     * Get the profile a user has
     */
    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    /**
    * Get the work experiences a user has
    */
    public function work_experiences()
    {
        return $this->hasMany(WorkExperience::class);
    }

    /**
     * Get the roles a user has
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role', 'user_id', 'role_id');
    }

    public function hasAnyRole($roles)
    {
        if (!is_array($roles)) {
            $roles = [$roles]; 
        }
        if ($this->roles()->whereIn('name', $roles)->first()) { 
            return true;
        }
        return false; 
    }

    /**
     * Get user with role assigned
     */
    public function isRole($roleName)
    {
        foreach ($this->roles()->get() as $role)
        {
            if ($role->name == $roleName)
            {
                return true;
            }
        }
        return false;
    }
}
