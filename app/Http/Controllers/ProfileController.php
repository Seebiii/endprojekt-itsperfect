<?php

namespace App\Http\Controllers;

use App\Technology;
use App\Profile;
use App\WorkExperience;
use App\User;
use App\State;
use App\Employment;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Profile $profile, WorkExperience $workExperience)
    {
        $profile = $profile->where('user_id', auth()->id())->first();

        $workExperience = $workExperience->where('user_id', auth()->id())->get();

        return view('backend.profile.index', [
            'profile' => $profile,
            'workExperience' => $workExperience
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Profile $profile)
    {
        $states = State::all();
        $employments = Employment::all();
        $technologies = Technology::all();
        
        if( ($profile->where('user_id', auth()->id())->first()) != null ){
            return redirect()->route('profile.index');
        }

        return view('backend.profile.create', [
            'states' => $states,
            'employments' => $employments,
            'technologies' => $technologies
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request, [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'state' => 'required',
            'age' => 'required',
            'curriculum_vitae' => 'required',
            'about_me' => 'required',
            'good_knowledge' => 'required',
            'average_knowledge' => 'required'
        ]);

        $curriculum_vitae_file = time() . '.' . $request->file('curriculum_vitae')->getClientOriginalExtension();
        $request->file('curriculum_vitae')->storeAs('public/curriculum_vitae', $curriculum_vitae_file);

        $profile = Profile::create([
            'name' => request('name'),
            'surname' => request('surname'),
            'state_id' => request('state'),
            'age' => request('age'),
            'curriculum_vitae' => $curriculum_vitae_file,
            'about_me' => request('about_me'),
            'good_knowledge' => request('good_knowledge'),
            'average_knowledge' => request('average_knowledge'),
            'user_id' => auth()->id(),
        ]);

        for($i = 0; $i < count($request['company']); $i++) {
            $work_experiences = new WorkExperience;
            $work_experiences->company = $request['company'][$i];
            $work_experiences->occupational_title = $request['occupational_title'][$i];
            $work_experiences->description = $request['description'][$i];
            $work_experiences->period_to = Carbon::parse($request['period_to'][$i]);
            $work_experiences->period_from = Carbon::parse($request['period_from'][$i]);
            $work_experiences->user_id = auth()->id();
            $work_experiences->save();
        }

        $profile->technologies()->attach($request->input('good_knowledge'));
        $profile->technologies()->attach($request->input('average_knowledge'));

        return redirect()->route('profile.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        $states = State::all();
        $employments = Employment::all();
        $technologies = Technology::all();

        $user = User::where('id', auth()->id())->first();

        return view('backend.profile.edit', [
            'profile' => $profile,
            'states' => $states,
            'employments' => $employments,
            'technologies' => $technologies,
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'state' => 'required',
            'age' => 'required',
            'about_me' => 'required',
            'good_knowledge' => 'required',
            'average_knowledge' => 'required'
        ]);

        $profile->name = $request->name;
        $profile->surname = $request->surname;
        $profile->state_id = $request->state;
        $profile->age = $request->age;
        $profile->about_me = $request->about_me;
        $profile->good_knowledge = $request->good_knowledge;
        $profile->average_knowledge = $request->average_knowledge;
        $profile->save();

        $workExperience = WorkExperience::where('user_id', auth()->id())->delete();

        for($i = 0; $i < count($request['company']); $i++) {
            $work_experiences = new WorkExperience;
            $work_experiences->company = $request['company'][$i];
            $work_experiences->occupational_title = $request['occupational_title'][$i];
            $work_experiences->description = $request['description'][$i];
            $work_experiences->period_to = Carbon::parse($request['period_to'][$i]);
            $work_experiences->period_from = Carbon::parse($request['period_from'][$i]);
            $work_experiences->user_id = auth()->id();
            $work_experiences->save();
        }

        $profile->technologies()->detach();
        $profile->technologies()->attach($request->input('good_knowledge'));
        $profile->technologies()->attach($request->input('average_knowledge'));

        return redirect()->route('profile.index')->with('success', 'Your profile was successfully saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        $profile->delete();
        $workExperience = WorkExperience::where('user_id', auth()->id())->delete();

        return redirect()->route('dashboard')->with('success', 'Profile was successfully deleted!');
    }
}
