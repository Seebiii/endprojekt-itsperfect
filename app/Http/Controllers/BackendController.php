<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Role;
use Illuminate\Http\Request;

class BackendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        return view('backend.index', compact('user'));
    }

    /**
     * Show the application users.
     *
     * @return \Illuminate\Http\Response
     */
    public function users()
    {
        $users = User::all();

        return view('backend.users.index', compact('users'));
    }

    public function postAdminAssignRoles(Request $request)
    {
        $user = User::where('email', $request['email'])->first();

        $user->roles()->detach();

        if ($request['role_employer']) {
            $user->roles()->attach(Role::where('name', 'employer')->first());
        }

        if ($request['role_employee']) {
            $user->roles()->attach(Role::where('name', 'employee')->first());
        }

        if ($request['role_admin']) {
            $user->roles()->attach(Role::where('name', 'admin')->first());
        }

        return redirect()->back();
    }
}
