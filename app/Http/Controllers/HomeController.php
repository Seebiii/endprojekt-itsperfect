<?php

namespace App\Http\Controllers;

use App\Job;
use App\State;
use App\Technology;
use App\Employment;
use App\Profile;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
		$states = State::all();
		$technologies = Technology::all();
		$employment = Employment::all();

		return view('pages.home', [
			'states' => $states,
			'technologies' => $technologies,
			'employment' => $employment,
		]);
    }
}
