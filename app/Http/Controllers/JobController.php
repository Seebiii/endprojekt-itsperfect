<?php

namespace App\Http\Controllers;

use App\Job;
use App\Profile;
use App\State;
use App\Technology;
use App\User;
use App\Employment;
use Auth;
use Mail;
use Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JobController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Job $jobs)
    {
        $jobs = $jobs->where('user_id', auth()->id())->get();

        return view('backend.job.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = State::all();
        $technologies = Technology::all();
        $employments = Employment::all();

        session()->flash('success', 'Job was successfully created!');

        return view('backend.job.create', [
            "states" => $states,
            "technologies" => $technologies,
            "employments" => $employments
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required',
            'state' => 'required',
            'employment' => 'required',
            'required_knowledge' => 'required',
            'desired_knowledge' => 'required'
        ]);

        $job = Job::create([
            'title' => request('title'),
            'description' => request('description'),
            'state_id' => request('state'),
            'employment_id' => request('employment'),
            'is_active' => 1,
            'is_top_job' => 0,
            'required_knowledge' => request('required_knowledge'),
            'desired_knowledge' => request('desired_knowledge'),
            'user_id' => auth()->id(),
        ]);

        $job->technologies()->attach($request->input('required_knowledge'));
        $job->technologies()->attach($request->input('desired_knowledge'));

        session()->flash('success', 'The job was successfully saved!');

        return redirect()->route('job.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job, Profile $profile)
    {
        $profile = $profile->where('user_id', auth()->id())->first();
        $states = State::all();
        $technologies = Technology::all();

        return view('pages.job.show', [
            'job' => $job,
            'profile' => $profile,
            'states' => $states,
            'technologies' => $technologies
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {
        $states = State::all();
        $technologies = Technology::all();
        $employments = Employment::all();

        return view('backend.job.edit', [
            "job" => $job,
            "states" => $states,
            "technologies" => $technologies,
            "employments" => $employments
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required',
            'state' => 'required',
            'employment' => 'required',
            'required_knowledge' => 'required',
            'desired_knowledge' => 'required',
        ]);

        $job->title = $request->title;
        $job->description = $request->description;
        $job->state_id = $request->state;
        $job->employment_id = $request->employment;
        $job->required_knowledge = $request->required_knowledge;
        $job->desired_knowledge = $request->desired_knowledge;
        $job->save();

        $job->technologies()->detach();
        $job->technologies()->attach($request->input('required_knowledge'));
        $job->technologies()->attach($request->input('desired_knowledge'));

        return redirect()->route('job.index')->with('success', 'Your job was successfully saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {
        $job->delete();
        $job->technologies()->detach();

        return redirect()->route('job.index')->with('success', 'Job was successfully deleted!');
    }

    /**
     * Update status.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function toggleIsActive(Job $job)
    {
        $job->is_active = !$job->is_active;
        $job->save();

        return redirect()->route('job.index')->with('success', 'Job status changed!');
    }

    /**
     * Send job application.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function sendJobApplication(Request $request, Job $job, Profile $profile)
    {
        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email',
            'msg' => 'required|min: 10',
            ''
        ]);

        // get employer data
        $user = User::findOrFail($job->user_id);

        // get employee profile data
        $profile = $profile->where('user_id', auth()->id())->get()[0];

        // get curriculum vitae path
        $file_path = public_path('storage/curriculum_vitae/') . $profile->curriculum_vitae;

        $data = array(
            'name' => $request->name,
            'surname' => $request->surname,
            'email' => $request->email,
            'msg' => $request->msg,
            'message_to' => $user->email,
            'subject' => $job->title,
            'curriculum_vitae' => $file_path,
            'state' => $request->state,
            'good_knowledge' => $request->good_knowledge,
            'average_knowledge' => $request->average_knowledge
        );

        Mail::send('emails.application', $data, function($message) use ($data) {
            $message->from($data['email']);
            $message->to($data['message_to']);
            $message->subject('Job application - ' . $data['subject']);
            $message->attach($data['curriculum_vitae'], [
                'as' => 'curriculum_vitae_' . $data['name'] . '_' . $data['surname'],
                'mime' => 'application/pdf'
            ]);
        });

        session()->flash('success', 'Your email was sent!');

        return back();
    }
}
