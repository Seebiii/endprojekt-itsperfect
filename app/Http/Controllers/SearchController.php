<?php

namespace App\Http\Controllers;

use App\State;
use App\Technology;
use App\Employment;
use App\Profile;
use Illuminate\Http\Request;
use App\JobSearch\JobSearch;
use App\JobCalculation\JobCalculation;
use Auth;

class SearchController extends Controller
{	
	public static function filter(Request $request)
	{	
		$states = State::all();
		$technologies = Technology::all();
		$employment = Employment::all();
		$profile = Profile::where('user_id', auth()->id())->first();

		$jobs = JobSearch::apply($request);

		if(Auth::check() && Auth::user()->isRole('employee') && $profile != null) {
			new JobCalculation($jobs);
		}

		return view('pages.home', [
			'states' => $states,
			'technologies' => $technologies,
			'employment' => $employment,
			'profile' => $profile
		])->with('jobs', $jobs);
	}
}