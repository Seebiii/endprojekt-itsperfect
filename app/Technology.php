<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Technology extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'name',
	];

	/**
     * Get jobs with a certain tag
     */
	public function jobs()
	{
		return $this->belongsToMany(Job::class);
	}

	/**
     * Get profiles with a certain tag
     */
	public function profiles()
	{
		return $this->belongsToMany(Profile::class);
	}
}
