<?php

namespace App\JobSearch\Filters;

use Illuminate\Database\Eloquent\Builder;

class Employment implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     * 
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
		return $builder->whereIn('employment_id', $value);
    }
}