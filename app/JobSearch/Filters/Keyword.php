<?php

namespace App\JobSearch\Filters;

use Illuminate\Database\Eloquent\Builder;

class Keyword implements Filter
{
	/**
	 * Apply a given search value to the builder instance.
	 * 
	 * @param Builder $builder
	 * @param mixed $value
	 * @return Builder $builder
	 */
	public static function apply(Builder $builder, $value)
	{
		$values = static::formatValue($value);

		return $builder->where(function ($query) use ($values) {
			foreach($values as $value) {
				$query->where('title', 'LIKE', "%{$value}%")
					 ->orWhere('description', 'LIKE', "%{$value}%");
			}
		});
	}

	private static function formatValue($val)
	{
		return preg_split('/\s+/', $val, -1, PREG_SPLIT_NO_EMPTY);
	}
}