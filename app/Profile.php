<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'name',
		'surname',
		'state_id',
		'age',
		'curriculum_vitae',
		'about_me',
		'good_knowledge',
		'average_knowledge',
		'user_id'
	];

	/**
     * The attributes that should be cast to native types.
     *
     * @var arra
     */
	protected $casts = [
		'good_knowledge' => 'json',
		'average_knowledge' => 'json'
	];

	/**
	* Get the programming languages a profile has
	*/
	public function technologies()
	{
		return $this->belongsToMany(Technology::class);
	}

	/**
	* Get the user a profile has
	*/
	public function user()
	{
		return $this->hasOne(User::class);
	}
}
