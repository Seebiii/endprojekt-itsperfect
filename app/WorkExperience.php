<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkExperience extends Model
{
    /**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'company',
		'occupational_title',
		'description',
		'period_from',
		'period_to',
		'user_id'
	];

	/**
	* Get the profile that work experiences have
	*/
	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
