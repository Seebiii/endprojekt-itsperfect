<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
	/**
	* Set timestamps off
	*/
	public $timestamps = false;
	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'name',
	];

	/**
     * Get jobs with a certain state
     */
	public function jobs()
	{
		return $this->hasMany(Job::class);
	}
}
