<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employment extends Model
{
	/**
	* Set timestamps off
	*/
	public $timestamps = false;
	
    /**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'name',
	];

	/**
     * Get jobs with a certain employment
     */
	public function jobs()
	{
		return $this->hasMany(Job::class);
	}
}
