<?php 

namespace App\JobCalculation;

use App\Profile;

class JobCalculation
{
	public function __construct($jobs)
	{
		$job_calculation = array();

		// loop job collections
		for($i = 0; $i < count($jobs); $i++){

			// sum required and desired knowledge from job
			$job_sum = count($this->getJobRequiredKnowledge($jobs, $i)) + count($this->getJobDesiredKnowledge($jobs, $i)) / 2;

			// compare employee knowledge with job knowledge and count similarities
			$similarities_sum = count(array_intersect( $this->getJobRequiredKnowledge($jobs, $i), $this->getEmployeeGoodKnowledge() )) + 
					  		count(array_intersect( $this->getJobDesiredKnowledge($jobs, $i), $this->getEmployeeAverageKnowledge() )) / 2;

			// push percentage values to array
			array_push($job_calculation, $this->calculatePercentage($similarities_sum, $job_sum));

			// add new attribute field to collection
			$jobs[$i]['calculation'] = $job_calculation[$i];
		}
	}

	public function getEmployeeGoodKnowledge()
	{	
		$profile = Profile::where('user_id', auth()->id())->first();

		return $profile->good_knowledge;
	}

	public function getEmployeeAverageKnowledge()
	{	
		$profile = Profile::where('user_id', auth()->id())->first();

		return $profile->average_knowledge;
	}

	public function getJobRequiredKnowledge($collection, $increment)
	{
		return $collection[$increment]->required_knowledge;
	}

	public function getJobDesiredKnowledge($collection, $increment)
	{
		return $collection[$increment]->desired_knowledge;
	}

	private function calculatePercentage($similiarties, $max_points)
	{
		return round($similiarties/$max_points*100);
	}
}