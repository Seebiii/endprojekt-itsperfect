<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title',
		'description',
		'required_knowledge',
		'desired_knowledge',
		'is_active',
        'is_top_job',
		'state_id',
		'user_id',
        'employment_id'
	];
	
	/**
     * The attributes that should be cast to native types.
     *
     * @var arra
     */
	protected $casts = [
		'required_knowledge' => 'json',
		'desired_knowledge' => 'json',
	];

	/**
     * Get the user a job has
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

	/**
     * Get the state a job has
     */
    public function state()
    {
        return $this->belongsTo(State::class);
    }

    /**
     * Get the programming languages a job has
     */
    public function technologies()
    {
        return $this->belongsToMany(Technology::class);
    }

    /**
     * Get the employment a job has
     */
    public function employment()
    {
        return $this->belongsTo(Employment::class);
    }
}
